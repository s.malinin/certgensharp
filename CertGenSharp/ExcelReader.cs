﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace CertGenSharp
{
    internal class ExcelReader
    {
        private ExcelPackage xlPackage;
        private ExcelWorksheet currentWorksheet;
        private int totalRows;
        private int totalColumns;
    
        public ExcelReader(string fileName)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage xlPackage = new ExcelPackage(new FileInfo(fileName));
            currentWorksheet = xlPackage.Workbook.Worksheets.First(); //select sheet here
            totalRows = currentWorksheet.Dimension.End.Row;
            totalColumns = currentWorksheet.Dimension.End.Column;
        }

        public List<Dictionary<string, string>> GetLinesAsDict()
        {
            List<string> header = HeaderFields();
            List<Dictionary<string, string>> Lines = new List<Dictionary<string, string>>();

            for (int rowNum = 2; rowNum <= totalRows; rowNum++) //select starting row here
            {
                var row = currentWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                Dictionary<string, string> line = new Dictionary<string, string>();
                List<string> rowList = row.ToList();

                for (int colNum = 0; colNum < totalColumns; colNum++)
                {
                    try
                    {
                        line.Add(header[colNum], rowList[colNum]);
                    }
                    catch 
                    { 
                        try
                        {
                            line.Add(colNum.ToString(), rowList[colNum]);
                        }
                        catch (ArgumentOutOfRangeException) { }
                    }
                }
                Lines.Add(line);
            }

            return Lines;
        }

        public string EnlistFields()
        {
            var header = currentWorksheet.Cells[1, 1, 1, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
            return string.Join(Environment.NewLine, header);
        }

        private List<string> HeaderFields()
        {
            IEnumerable<string> header = currentWorksheet.Cells[1, 1, 1, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
            return header.ToList();
        }
    }
}
