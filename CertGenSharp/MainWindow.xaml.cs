﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;
using Microsoft.Win32;
using System.Threading;

namespace CertGenSharp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        private string pdfTemplate;
        private string partList;
        private string destFolder;
        private string embedableFont;

        public MainWindow()
        {
            InitializeComponent();
            ListFonts();
            btnPDFTemplateOpen.Click += BtnPDFTemplateOpen_Click;
            btnPartListOpen.Click += BtnPartListOpen_Click;
            btnChooseDestFolder.Click += BtnChooseDestFolder_Click;
            btnStart.Click += BtnStart_Click;
             
        }

        private void BtnPartListOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                partList = openFileDialog.FileName;
                tbPartList.Text = LastNSymbols(partList, 60);
            }
            ExcelReader xlr = new ExcelReader(partList);
            tbPartListFields.Text = xlr.EnlistFields();
        }

        private void BtnChooseDestFolder_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog folderBrowser = new OpenFileDialog();
            // Set validate names and check file exists to false otherwise windows will
            // not let you select "Folder Selection."
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            // Always default to Folder Selection.
            folderBrowser.FileName = "Folder Selection.";
            if (folderBrowser.ShowDialog() == true)
            {
                destFolder = Path.GetDirectoryName(folderBrowser.FileName);
                tbDestFolder.Text = LastNSymbols(destFolder, 60);
            }
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            tbDebug.Text += GetDebugTimestamp() + "Запуск" + Environment.NewLine;
            embedableFont = cbFontSelector.SelectedItem.ToString();
            var generatorThread = new Thread(RunPDFGenerator);
            generatorThread.Start();
        }

        private void RunPDFGenerator()
        {
            Dispatcher.BeginInvoke((Action)(() => UpdateDebugInfo(GetDebugTimestamp() + "Загрузка шаблона" + Environment.NewLine)));
            PDFGenerator pdfGenerator = new PDFGenerator(pdfTemplate);
            Dispatcher.BeginInvoke((Action)(() => UpdateDebugInfo(GetDebugTimestamp() + "Загрузка списка" + Environment.NewLine)));
            ExcelReader xlr = new ExcelReader(partList);
            List<Dictionary<string, string>> Lines = xlr.GetLinesAsDict();
            int counter = 0;
            string result;
            Dispatcher.BeginInvoke((Action)(() => UpdateDebugInfo(GetDebugTimestamp() + "Создание сертификатов" + Environment.NewLine)));
            foreach (var line in Lines)
            {
                try
                {
                    result = pdfGenerator.Generate(line, destFolder, embedableFont) + Environment.NewLine;
                }
                catch (Exception ex)
                {
                    string badLine;
                    line.TryGetValue("name", out badLine);
                    Dispatcher.BeginInvoke((Action)(() => UpdateDebugInfo(GetDebugTimestamp() + "Ошибка в строке: " + badLine + Environment.NewLine)));
                }
                counter++;
            }
            Dispatcher.BeginInvoke((Action)(() => UpdateDebugInfo(GetDebugTimestamp() + "Завершено. Создано " + counter + " сертификатов" + Environment.NewLine)));
        }

        private void BtnPDFTemplateOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "PDF files (*.pdf)|*.pdf|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                pdfTemplate = openFileDialog.FileName;
                tbPDFTemplate.Text = LastNSymbols(pdfTemplate, 60);
            }
            PDFGenerator pdfGenerator = new PDFGenerator(pdfTemplate);
            tbPDFTemplateFields.Text = pdfGenerator.EnlistFields();
        }

        private void ListFonts()
        {
            string[] fileArray = Directory.GetFiles(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Microsoft\\Windows\\Fonts"));
            foreach (var font in fileArray)
            {
                cbFontSelector.Items.Add(Path.GetFileName(font));
            }
            cbFontSelector.SelectedIndex = 0;
        }

        private string LastNSymbols(string source, int n)
        {
            if (source.Length > n)
            {
                return "..." + source.Substring(source.Length - n);
            }
            return source;
        }

        private void UpdateDebugInfo(string text)
        {
            tbDebug.Text += text;
        }

        private string GetDebugTimestamp()
        {
            var dt = DateTime.Now;
            return "[" + dt.ToString("HH:mm:ss.fff") + "] ";
        }
    }
}
