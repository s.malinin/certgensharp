﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using System.Text.Encodings;
using System.Reflection;

namespace CertGenSharp
{
    internal class PDFGenerator
    {
        private string pdfTemplateName;
        
        public PDFGenerator(string pdfName)
        {
            pdfTemplateName = pdfName;
        }

        public string EnlistFields()
        {
            PdfReader pdfReader = new PdfReader(pdfTemplateName);
            AcroFields pdfFormFields = pdfReader.AcroFields;
            StringBuilder fieldList = new StringBuilder();
            foreach (KeyValuePair<string, iTextSharp.text.pdf.AcroFields.Item> de in pdfReader.AcroFields.Fields)
            {
                fieldList.Append(de.Key.ToString() + Environment.NewLine);
            }
            return fieldList.ToString();
        }
        
        public string Generate(Dictionary<string, string> formData, string destFolder, string fontName)
        {
            string formDataItem;
            string newFile;

            if (!formData.TryGetValue("name", out newFile))
            {
                try
                {
                    newFile = formData.First().Value.ToString();
                }
                catch
                {
                    return "";
                }
                
            };
            if (newFile == string.Empty)
            {
                return "";
            }
            newFile = newFile.Replace("\"", string.Empty).Trim();
            newFile += ".pdf";

            PdfReader pdfReader = new PdfReader(pdfTemplateName);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Path.Combine(destFolder, newFile), FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            
            EmbedFont(fontName);

            foreach (KeyValuePair<string, iTextSharp.text.pdf.AcroFields.Item> de in pdfReader.AcroFields.Fields)
            {
                if (formData.TryGetValue(de.Key.ToString(), out formDataItem))
                {
                    pdfFormFields.SetField(de.Key.ToString(), formDataItem);

                }
            }
            pdfStamper.FormFlattening = true;
            pdfStamper.Close();

            return newFile;
        }

        private void EmbedFont(string fontName)
        {
            string fontFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Microsoft\\Windows\\Fonts", fontName);
            EncodingProvider encodingProvider = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(encodingProvider);
            BaseFont fontToEmbed = BaseFont.CreateFont(fontFileName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        }

    }
}
